package com.example.MyMusic.albums;

import com.example.MyMusic.albums.models.Album;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlbumRepository extends JpaRepository<Album,Long> {

    List<Album> findAllByOrderByTitleAsc();
}
