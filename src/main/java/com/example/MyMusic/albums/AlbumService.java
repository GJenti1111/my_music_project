package com.example.MyMusic.albums;

import com.example.MyMusic.albums.models.Album;

import java.util.List;

public interface AlbumService {

    List<Album> findAllByOrderByTitleAsc();
}
