package com.example.MyMusic.albums;

import com.example.MyMusic.albums.models.Album;
import com.example.MyMusic.albums.utils.AlbumMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlbumServiceImpl implements AlbumService {

    private AlbumRepository albumRepository;
    private AlbumMapper albumMapper;

    @Autowired
    public AlbumServiceImpl(AlbumRepository albumRepository, AlbumMapper albumMapper) {
        this.albumRepository = albumRepository;
        this.albumMapper = albumMapper;
    }

    @Override
    public List<Album> findAllByOrderByTitleAsc() {
        return albumRepository.findAllByOrderByTitleAsc();
    }
}
