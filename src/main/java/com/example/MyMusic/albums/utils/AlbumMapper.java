package com.example.MyMusic.albums.utils;

import com.example.MyMusic.albums.models.Album;
import com.example.MyMusic.albums.models.AlbumDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface AlbumMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateAlbumFromDto(AlbumDto dto, @MappingTarget Album album);
}
