package com.example.MyMusic.artists;

import com.example.MyMusic.artists.models.Artist;
import com.example.MyMusic.artists.models.ArtistDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/artists")
public class ArtistController {

    private ArtistService artistService;

    @Autowired
    public ArtistController(ArtistService artistService) {
        this.artistService = artistService;
    }

    @GetMapping
    public List<Artist> findAllByOrderByNameAsc() {
        return artistService.findAllByOrderByNameAsc();
    }

    @GetMapping(path = "/{id}")
    public Optional<Artist> findById(@PathVariable Long id) {
        return artistService.findById(id);
    }

    @PostMapping
    public Artist save(@RequestBody Artist artist) {
        return artistService.save(artist);
    }

    @PatchMapping(path = "/{id}")
    public Artist update(@PathVariable Long id, @RequestBody ArtistDto artistDto) {
        return artistService.update(id, artistDto);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteById(@PathVariable Long id) {
        artistService.deleteById(id);
    }
}
