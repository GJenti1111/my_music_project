package com.example.MyMusic.artists;

import com.example.MyMusic.artists.models.Artist;
import com.example.MyMusic.artists.models.ArtistDto;

import java.util.List;
import java.util.Optional;

public interface ArtistService {

    List<Artist> findAllByOrderByNameAsc();

    Optional<Artist> findById(Long id);

    Artist save(Artist artist);

    Artist update(Long id, ArtistDto artistDto);

    void deleteById(Long id);

}
