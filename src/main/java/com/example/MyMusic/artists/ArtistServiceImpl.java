package com.example.MyMusic.artists;

import com.example.MyMusic.artists.models.Artist;
import com.example.MyMusic.artists.models.ArtistDto;
import com.example.MyMusic.artists.utils.ArtistMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class ArtistServiceImpl implements ArtistService {

    private ArtistRepository artistRepository;
    private ArtistMapper artistMapper;

    @Autowired
    public ArtistServiceImpl(ArtistRepository artistRepository, ArtistMapper artistMapper) {
        this.artistRepository = artistRepository;
        this.artistMapper = artistMapper;
    }

    @Override
    public List<Artist> findAllByOrderByNameAsc() {
        return artistRepository.findAllByOrderByNameAsc();
    }

    @Override
    public Optional<Artist> findById(Long id) {
        Optional<Artist> optionalArtist = artistRepository.findById(id);
        if(optionalArtist.isPresent()) {
            return optionalArtist;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public Artist save(Artist artist) {
        return artistRepository.save(artist);
    }

    @Override
    public Artist update(Long id, ArtistDto artistDto) {
        Optional<Artist> optionalArtist = artistRepository.findById(id);
        if(optionalArtist.isPresent()) {
            Artist myArtist = optionalArtist.get();
            artistMapper.updateArtistFromDto(artistDto, myArtist);
            return artistRepository.save(myArtist);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public void deleteById(Long id) {
        Optional<Artist> optionalArtist = artistRepository.findById(id);
        if(optionalArtist.isPresent()) {
            Artist myArtist = optionalArtist.get();
            artistRepository.delete(myArtist);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
