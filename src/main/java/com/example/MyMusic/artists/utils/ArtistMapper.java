package com.example.MyMusic.artists.utils;

import com.example.MyMusic.artists.models.Artist;
import com.example.MyMusic.artists.models.ArtistDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface ArtistMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateArtistFromDto(ArtistDto dto, @MappingTarget Artist artist);
}
