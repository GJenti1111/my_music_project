package com.example.MyMusic.songs;

import com.example.MyMusic.songs.models.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(path = "/songs")
public class SongController {

    private SongService songService;

    @Autowired
    public SongController(SongService songService) {
        this.songService = songService;
    }
    @GetMapping()
    public Iterable<Song> findAll(){
        return songService.findAll();
    }
    @GetMapping("/{id}")
    public Optional<Song> findById(@PathVariable Long id) {
        return songService.findById(id);
    }

}
