package com.example.MyMusic.songs;

import com.example.MyMusic.songs.models.Song;
import org.springframework.data.repository.CrudRepository;

public interface SongRepository extends CrudRepository<Song,Long> {

}
