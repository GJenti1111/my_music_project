package com.example.MyMusic.songs;

import com.example.MyMusic.songs.models.Song;

import java.util.Optional;

public interface SongService {

    Iterable<Song> findAll();

    Optional<Song> findById(Long id);
}
